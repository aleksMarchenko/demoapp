CREATE DATABASE IF NOT EXISTS UserDb;

DROP TABLE IF EXISTS user;

CREATE TABLE IF NOT EXISTS user
(
    id      BIGINT       NOT NULL
        PRIMARY KEY,
    name    VARCHAR(255) NULL,
    surname VARCHAR(255) NULL
);

CREATE TABLE IF NOT EXISTS address
(
    address_type VARCHAR(31)  NOT NULL,
    id           BIGINT       NOT NULL
        PRIMARY KEY,
    details      VARCHAR(255) NULL,
    line1        VARCHAR(255) NULL,
    user_id      BIGINT       NOT NULL,
    CONSTRAINT FKda8tuywtf0gb6sedwk7la1pgi
        FOREIGN KEY (user_id) REFERENCES user (id)
);