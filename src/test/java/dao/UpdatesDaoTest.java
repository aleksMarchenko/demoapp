package dao;

import hib.DaoConfiguration;
import model.Address;
import model.Email;
import model.HomeAddress;
import model.User;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import service.UserService;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {DaoConfiguration.class})
public class UpdatesDaoTest {

    private User user;

    @Autowired
    private UserDao userDao;
    @Autowired
    private UserService userService;

    @Before
    public void setUp() {
        User newUser = new User("Ivan", "Ivanov");
        Address email = new Email();
        email.setDetails("ivanov@gmail.com");
        HomeAddress homeAddress = new HomeAddress();
        homeAddress.setDetails("someStreet");
        homeAddress.setLine1("1");
        newUser.setAddressList(Arrays.asList(email, homeAddress));
        user = userDao.save(newUser);
    }

    @After
    public void tearDown() {
        userDao.delete(user.getId());
    }

    @Test
    public void saveUser() {
        Optional<User> userDaoById = userDao.getById(user.getId());
        assertTrue(userDaoById.isPresent());
        equalsUsers(user, userDaoById.get());
    }

    @Test
    public void deleteUserById() {
        boolean delete = userDao.delete(user.getId());
        Optional<User> userById = userDao.getById(user.getId());
        assertTrue(delete);
        assertFalse(userById.isPresent());
        assertFalse(userDao.delete(user.getId()));
    }

    @Test
    public void getAll() {
        List<User> expected = Collections.singletonList(user);
        List<User> actual = userDao.getAll();
        assertThat(expected, is(actual));
    }

    @Test
    public void getById() {
        Optional<User> userById = userDao.getById(user.getId());
        assertEquals(user.getId(), userById.get().getId());
    }

    @Test
    public void simpleUpdate() {
        User newUser = new User("Dmytro", "Vasilev");
        newUser.setId(user.getId());
        User userDb = userService.updateUser(newUser);
        equalsUsers(newUser, userDb);
    }

    @Test
    public void complexUpdate() {
        User newUser = new User("newIvan", "newIvanow");
        newUser.setId(user.getId());
        Address email = new Email();
        email.setDetails("newivanov@gmail.com");
        HomeAddress homeAddress = new HomeAddress();
        homeAddress.setDetails("newsomeStreet");
        homeAddress.setLine1("2");
        newUser.setAddressList(Arrays.asList(email, homeAddress));
        User userDb = userService.updateUser(newUser);
        equalsUsers(newUser, userDb);

    }

    private void equalsUsers(User expectedUser, User actualUser) {
        assertEquals(expectedUser.getName(), actualUser.getName());
        assertEquals(expectedUser.getSurname(), actualUser.getSurname());
        List<Address> expectedAddress = expectedUser.getAddressList();
        List<Address> actualAddress = actualUser.getAddressList();
        assertThat("oops", expectedAddress, is(actualAddress));
    }
}
