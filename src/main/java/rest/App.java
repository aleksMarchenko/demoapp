package rest;

import controller.UserRestController;
import io.dropwizard.Application;
import io.dropwizard.Configuration;
import io.dropwizard.setup.Environment;

public class App extends Application<Configuration> {

    private final String PACKAGE_FOR_JERSEY_SCAN = "controller";

    @Override
    public void run(Configuration configuration, Environment environment) throws Exception {
        final UserRestController resource = new UserRestController();
        environment.jersey().packages(PACKAGE_FOR_JERSEY_SCAN);
    }

    public static void main(String[] args) throws Exception {
        new App().run(args);
    }
}
