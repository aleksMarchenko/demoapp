package service;

import model.Address;
import org.springframework.stereotype.Service;

@Service
public class AddressService {

    public void updateAddress(Address dbAddress, Address newAddress) {
        dbAddress.setDetails(newAddress.getDetails());
    }
}
