package service;

import dao.UserDao;
import model.Address;
import model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

@Service
public class UserService {

    private final UserDao userDao;
    private final AddressService addressService;

    @Autowired
    public UserService(UserDao userDao, AddressService addressService) {
        this.userDao = userDao;
        this.addressService = addressService;
    }

    @Transactional
    public User updateUser(User user) {
        Optional<User> maybeUser = userDao.getById(user.getId());
        User dbUser = maybeUser.orElseThrow(() -> new RuntimeException("user not found by id " + user.getId()));
        dbUser.setName(user.getName());
        dbUser.setSurname(user.getSurname());
        updateAddress(dbUser, user);
        return user;
    }

    private void updateAddress(User dbUser, User newUser) {

        Map<Long, Address> stillExistingAddresses = new HashMap<>();
        for (Address address1 : newUser.getAddressList()) {
            if (Objects.nonNull(address1.getId())) {
                if (stillExistingAddresses.put(address1.getId(), address1) != null) {
                    throw new IllegalStateException("Duplicate key");
                }
            }
        }

        List<Address> addressesToAdd = new ArrayList<>();
        for (Address address : newUser.getAddressList()) {
            if (Objects.isNull(address.getId())) {
                addressesToAdd.add(address);
            }
        }
        Iterator<Address> iterator = dbUser.getAddressList().iterator();

        while (iterator.hasNext()) {
            Address dbAddress = iterator.next();
            if (stillExistingAddresses.containsKey(dbAddress.getId())) {
                Address newAddress = stillExistingAddresses.get(dbAddress.getId());
                addressService.updateAddress(dbAddress, newAddress);
            } else {
                iterator.remove();
            }
        }
        dbUser.getAddressList().addAll(addressesToAdd);
    }
}
