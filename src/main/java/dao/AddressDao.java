package dao;

import model.Address;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Repository
public class AddressDao {

    private final SessionFactory sessionFactory;

    @Autowired
    public AddressDao(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Transactional
    public Address save(Address address) {
        Session session = sessionFactory.getCurrentSession();
        session.save(address);
        return address;
    }

    @Transactional
    public Optional<Address> getById(Long id) {
        Session session = sessionFactory.getCurrentSession();
        Address address = session.get(Address.class, id);
        return Optional.ofNullable(address);
    }

    @Transactional
    public boolean delete(Long id) {
        boolean result = false;
        Session session = sessionFactory.getCurrentSession();
        Optional<Address> addressById = getById(id);
        if (addressById.isPresent()) {
            session.delete(addressById.get());
            result = true;
        }
        return result;
    }
}
