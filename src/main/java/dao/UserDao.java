package dao;

import model.User;
import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.criteria.CriteriaQuery;
import java.util.List;
import java.util.Optional;

@Repository
public class UserDao {

    private final SessionFactory sessionFactory;

    @Autowired
    public UserDao(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Transactional
    public User save(User user) {
        Session currentSession = sessionFactory.getCurrentSession();
        currentSession.save(user);
        return user;
    }

    @Transactional(readOnly = true)
    @SuppressWarnings("unchecked")
    public List<User> getByIds(List<Long> ids) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("from user u where u.id in (:ids)");
        query.setParameterList("ids", ids);
        return query.getResultList();
    }

    @Transactional
    public Optional<User> getById(Long id) {
        Session session = sessionFactory.getCurrentSession();
        User user = session.get(User.class, id);
        if (null != user) {
            Hibernate.initialize(user.getAddressList());
        }
        return Optional.ofNullable(user);
    }

    @Transactional(readOnly = true)
    public List<User> getAll() {
        Session session = sessionFactory.getCurrentSession();
        CriteriaQuery<User> query = session.getCriteriaBuilder().createQuery(User.class);
        query.from(User.class);
        return session.createQuery(query).getResultList();
    }

    @Transactional
    public boolean delete(Long id) {
        boolean result = false;
        Session session = sessionFactory.getCurrentSession();
        Optional<User> userById = getById(id);
        if (userById.isPresent()) {
            session.delete(userById.get());
            result = true;
        }
        return result;
    }
}
